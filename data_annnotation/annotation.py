import sys
import os
import json
import GEOparse
from GEOparse.logger import set_verbosity
from Levenshtein import distance
#import pysemares as psa
import yaml

import semares_bl

config_dir = "./configs"
if not os.path.isdir(config_dir):
    os.mkdir(config_dir)

parsed_gses = {}

def get_empty_config(gse):
   return {"gse": gse, "field_map": {}}

def get_config(conf_name):
   conf_filename = config_dir + "/" + conf_name + ".yaml"
   if os.path.isfile(conf_filename):
       with open(conf_filename, 'r') as file:
           config = yaml.safe_load(file)
           if config is None:
              config = get_empty_config(conf_name)
   else:
       config = get_empty_config(conf_name)
   return config 
   
def save_conf(conf):
    conf_name = conf["gse"]
    conf_filename = config_dir + "/" + conf_name + ".yaml"
    config_f = open(conf_filename, "w")
    conf_new = conf.copy()
#    conf_new["field_map"] = {conf_new["field_map"][key]: key for key in conf_new["field_map"]}
    yaml.dump(conf_new, config_f) 
    config_f.close()

def get_gsm_metadata(gse_str):
    global parsed_gses
    set_verbosity("INFO")
    if gse_str in parsed_gses:
       gse = parsed_gses[gse_str]
    else:
       gse = GEOparse.get_GEO(geo=gse_str, destdir="./")
       parsed_gses[gse_str] = gse
    gsm_metadata = {}
    for gsm_name, gsm in gse.gsms.items():    
         gsm_metadata[gsm_name] = {}
         for ch1 in gsm.metadata["characteristics_ch1"]:
             (key,value) = ch1.split(":")
             key = key.strip()
             value = value.strip()
             gsm_metadata[gsm_name][key] = value
    return gsm_metadata
    
def get_metadata_summary(gsm_metadata):
    metadata_summary = {}    
    for gsm in gsm_metadata:
        for key in gsm_metadata[gsm]:
            value = gsm_metadata[gsm][key]
            if not key in metadata_summary:
                metadata_summary[key] = {}
            metadata_summary[key][value] = 1
    return metadata_summary        
    

def get_template_fields():
   return {"sex", "age", "disease", "tissue", "origin", "diagnostic_method"}
#    return {"Organism", "Source", "Sex", "Tissue", "Age", "Disease", "Disease state", "Diagnostics", "Treatment Drug", "Time point", "Phenotype", "Subject ID"}
def load_synonyms(key=None):
   synonyms_filename = config_dir + "/synonyms.json"
   if os.path.isfile(synonyms_filename):
       synonyms_f = open(synonyms_filename, "r")
       synonyms = json.load(synonyms_f)
   else:
       synonyms = {}
   if not key:
       return synonyms
   if key in synonyms:
       return synonyms[key]
   else:
       return {}
       
def add_synonym(key, value, synonym):
   synonyms = load_synonyms()
   if not key in synonyms:
       synonyms[key] = {}
   if not value in synonyms[key]:
       synonyms[key][value] = []
   if not synonym in synonyms[key][value]:
       synonyms[key][value].append(synonym)
       synonyms_filename = config_dir + "/synonyms.json"
       synonyms_f = open(synonyms_filename, "w")
       json.dump(synonyms, synonyms_f, indent = 3) 
       synonyms_f.close()

def get_value_suggestion(key, value = None):
    print(value)
    synonyms = load_synonyms(key)
    if not value:
        return list(synonyms.keys())
    for cur_value in synonyms:
        if value.lower() == cur_value.lower():
            return cur_value
        else:
            for synonym in synonyms[cur_value]:
                 if value.lower() == synonym.lower():
                      return cur_value
    return None

def apply_key_mapping(metadata, key_mapping):
    res_meta = {}
    mapped_keys = {}
    for gsm in metadata:
        res_meta[gsm] = {}
#        for key in key_mapping:
#            old_key = key_mapping[key]
#            if old_key in metadata[gsm]:
#                res_meta[gsm][key] = metadata[gsm][old_key]
#                mapped_keys[old_key] = 1
#        for key in metadata[gsm]:
#            if not key in mapped_keys:
#                res_meta[gsm][key] = metadata[gsm][key]        
        for key in metadata[gsm]:
            if key in key_mapping:
                res_meta[gsm][key_mapping[key]] = metadata[gsm][key] 
    return res_meta


def check_gse(conf, conf_name):
  if not "gse" in conf:
      if conf_name.startswith("GSE"):
           add_gse(conf_name, conf_name)
      print(f"GSE is not set in config {conf_name}")
      print("run annotation.py add_gse <GSE>")
      sys.exit()

def apply_value_mapping(metadata, value_mapping, na_strings = ["NA","na","N/A","n/a"]):
    res_meta = {}
    for gsm in metadata:
        res_meta[gsm] = {}
        for key in metadata[gsm]:
            value = metadata[gsm][key]
            if value in na_strings:
#                del res_meta[gsm][key]
                continue
            if not key in value_mapping:
                 res_meta[gsm][key] = value      
                 continue
#            res_meta[gsm][key] = value

#            if key in value_mapping:
            if value in value_mapping[key]:
                 new_value = value_mapping[key][value]
                 res_meta[gsm][key] = new_value
#            else:
#                print(f"WARNING: {value} not normalized")
    return res_meta

def apply_default_values(metadata, defaults):
    for gsm in metadata:
        for key in defaults:
            metadata[gsm][key] = defaults[key]
    return metadata

def list_annotations(conf_name):
    conf = get_config(conf_name)
    gsm_metadata = get_annotations(conf_name)
    metadata_summary = get_metadata_summary(gsm_metadata)
    template_fields = get_template_fields()
    shown_fields = []
    print("********************in template (mapped)*******************")
    field_map_rev = {conf["field_map"][key]: key for key in conf["field_map"]}
    for key in metadata_summary:
        if key in template_fields:
             orig_field = field_map_rev.get(key,key)
             print(key,f"({orig_field})",":", "; ".join(sorted(metadata_summary[key])))
             shown_fields.append(key)
    print("********************in template (not mapped)*******************")
    for key in template_fields:
        if not key in shown_fields:
             print(key)
    print("********************not in template*******************")
    metadata_orig = get_gsm_metadata(conf["gse"])
    metadata_orig_summary = get_metadata_summary(metadata_orig)
    for key in metadata_orig_summary:
        if key not in template_fields and key not in conf["field_map"]:
             print(key,":", "; ".join(sorted(metadata_orig_summary[key])))

def list_annotations_tabular(conf_name):
    gsm_metadata = get_annotations(conf_name)    
    print(gsm_metadata)
    template_fields = get_template_fields()
    out_list = ["GSM"]
    format_list = ["%-16s"]
    for field in template_fields:
        out_list.append(field)
        field_len = len(field)+4
        format_list.append("%-"+str(field_len)+"s")
    print("".join(format_list)%tuple(out_list))
    for meta_row in gsm_metadata:
         out_list = [meta_row]
         for field in template_fields:
               if field in gsm_metadata[meta_row]:
                   out_list.append(gsm_metadata[meta_row][field])
               else:
                   out_list.append("")
         print("".join(format_list)%tuple(out_list))
         


def get_annotations(conf_name):
    conf = get_config(conf_name)
    check_gse(conf, conf_name)
    gsm_metadata = get_gsm_metadata(conf["gse"])
    if "field_map" in conf:
        gsm_metadata = apply_key_mapping(gsm_metadata, conf["field_map"])
    if "value_map" in conf:
        gsm_metadata = apply_value_mapping(gsm_metadata, conf["value_map"])
    if "default_values" in conf:
        gsm_metadata = apply_default_values(gsm_metadata, conf["default_values"])
    return gsm_metadata
    
    
def get_field_score(field, key):
#    print(field,key)
    if field == key:
        return 2
    if field.lower() == key.lower():
        return 1.5
    dist = distance(field.lower(), key.lower())
    if len(field) > len(key):
        score = (len(field)-dist) / len(field)
    else:
        score = (len(key)-dist) / len(field)
#    print(dist)
#    print(score)
    return score
      

def get_mapping_suggestion(conf, field, gsm_metadata):
    max_score = 0
    max_key = ""
    for key in gsm_metadata:
        cur_score = get_field_score(field, key)
        if cur_score>max_score:
             max_score = cur_score
             max_key = key
    return max_key
    
      
def auto_key_mapping(conf_name):
    template_fields = get_template_fields()
    conf = get_config(conf_name)
    check_gse(conf, conf_name)
    gsm_metadata = get_gsm_metadata(conf["gse"])
    metadata_summary = get_metadata_summary(gsm_metadata)
    mapping_map = {}
#    for key in template_fields:
    for key in metadata_summary:
#        mapping_suggestion = get_mapping_suggestion(conf, key, metadata_summary)
        mapping_suggestion = get_mapping_suggestion(conf, key, template_fields)
        print("Values: %s"%"; ".join(sorted(metadata_summary[key])))
#        print([f"{ind}: {field}" for (ind,field) in enumerate(metadata_summary)]+["space: confirm", "other symbol: skip"])
        print([f"{ind}: {field}" for (ind,field) in enumerate(template_fields)]+["space: confirm", "other symbol: skip"])
        user_input = input(f"{key}[{mapping_suggestion}]>")
        if user_input == "":
            mapping = mapping_suggestion
#        elif user_input in [str(ind) for (ind,field) in enumerate(metadata_summary)]:
        elif user_input in [str(ind) for (ind,field) in enumerate(template_fields)]:
#            mapping = list(metadata_summary.keys())[int(user_input)]
             mapping = list(template_fields)[int(user_input)]
        elif user_input == "-1":
            mapping = None
        else:
            mapping = user_input
        if mapping:
            mapping_map[key] = mapping
#    print(mapping_map)
    conf["field_map"] = mapping_map
    save_conf(conf)
    list_annotations(conf_name)

def auto_value_mapping(conf_name, key):
    conf = get_config(conf_name)
    gsm_metadata = get_gsm_metadata(conf["gse"])
    if "field_map" in conf:
        gsm_metadata = apply_key_mapping(gsm_metadata, conf["field_map"])
    metadata_summary = get_metadata_summary(gsm_metadata)
    default_suggestions = get_value_suggestion(key)
    mapping_map = {}
    for value in metadata_summary[key]:
        suggestion = get_value_suggestion(key, value)
        print([f"{ind}: {field}" for (ind,field) in enumerate(default_suggestions)]+["enter: confirm", "-1: skip"])
        user_input = input(f"{value}[{suggestion}]>")
        if user_input == "":
            mapping = suggestion
        elif user_input in [str(ind) for (ind,field) in enumerate(default_suggestions)]:
            mapping = list(default_suggestions)[int(user_input)]
        elif user_input == "-1":
            mapping = None
        else:
            mapping = user_input
        if mapping:
            mapping_map[value] = mapping
            add_synonym(key, mapping, value)
    print(mapping_map)
    if not "value_map" in conf:
        conf["value_map"] = {}
    conf["value_map"][key] = mapping_map
    save_conf(conf)

def add_default_value(conf_name, key, value):
    conf = get_config(conf_name)
    if not "default_values" in conf:
        conf["default_values"] = {}
    conf["default_values"][key] = value
    save_conf(conf)

def get_metadata_summary_grouped(gsm_metadata, group_key):
    metadata_summary = {}
    for gsm in gsm_metadata:
        if group_key in gsm_metadata[gsm]:
            grouped_value = gsm_metadata[gsm][group_key]
        else:
            grouped_value = "NA"
        if not grouped_value in metadata_summary:
            metadata_summary[grouped_value] = {}
        for key in gsm_metadata[gsm]:
            if key == group_key:
                continue
            value = gsm_metadata[gsm][key]
            if not key in metadata_summary[grouped_value]:
                metadata_summary[grouped_value][key] = {}
            metadata_summary[grouped_value][key][value] = 1
    return metadata_summary

def find_key_value(gsm_metadata, key, value):
    res_gsm = []
    for gsm in gsm_metadata:
        if key in gsm_metadata[gsm]:
             if gsm_metadata[gsm][key] == value:
                  res_gsm.append(gsm)
        else:
            if value is None:
               res_gsm.append(gsm)
    return res_gsm

def add_default_value_interactive(conf_name, key):
    conf = get_config(conf_name)
    while True:
        gsm_metadata = get_annotations(conf_name)
        meta_summary = get_metadata_summary_grouped(gsm_metadata, key)
        print("Summary for %s"%key)
        for group_value in meta_summary:  
            print("*********%s******"%group_value)
            out_list = []
            for cur_key in meta_summary[group_value]:
                out_list.append(cur_key+":"+ ";".join(meta_summary[group_value][cur_key].keys()))
            print("......".join(out_list))
        user_key = input(f"Enter filter key (all - all values, na - all without default value, exit - stop)>")
        if user_key == "exit":
            break
        if user_key == "na":
            gsm_list = find_key_value(gsm_metadata, key, None)
        elif user_key == "all":
            gsm_list = list(gsm_metadata.keys())
        else:
            user_value = input(f"Enter filter value (exit - stop)>")
            if user_value == "exit":
                break
            gsm_list = find_key_value(gsm_metadata, user_key, user_value)
        print(str(len(gsm_list))+" found")
        user_default_value = input(f"Enter default value (exit - stop)>")
        def_value_map = {gsm: user_default_value for gsm in gsm_list}
        if user_default_value == "exit":
            break
        if not "default_values" in conf:
           conf["default_values"] = {}
        if not key in conf["default_values"]:
             conf["default_values"][key] = {}
        conf["default_values"][key].update(def_value_map)
        save_conf(conf)

def add_gse(gse):
    conf = get_config(gse)
    save_conf(conf) 

#from pysemares.hierarchical_objects import ProjectHL
#from pysemares.hierarchical_objects import ExperimentHL
#from pysemares.hierarchical_objects import SampleHL
#from pysemares.hierarchical_objects import DatasetHL
#from pysemares.hierarchical_objects import DataobjectHL


def connect_semares(semares_api_host, semares_token):
    con = psa.semares_connect(
        api_path=semares_api_host,
        access_token=semares_token,
        DEBUG=False,
        settings_dict={"SSL_VERIFY":False},
    )
    return con    
    
def upload_samples_to_semares(con, gsm_metadata, project, experiment, experiment_type, experiment_template, sample_template):
    proj = ProjectHL(con = con, name=project)
    exp = ExperimentHL(con = con, name=experiment, parent = proj, type_name = experiment_type)
    exp.set_experiment_metadata_template(experiment_template)
    exp.set_sample_metadata_template(sample_template)
    for gsm in gsm_metadata:
         samp = SampleHL(con = con, name=gsm, parent = exp)
         samp.set_metadata(gsm_metadata[gsm], validate_dictionaries = False, validate_ontologies = False)

    proj.push_semares() 

gse = None

def input_gse():
   global gse
   if gse:
       return gse
   else:
       gse = input("gse>")
       return gse


#if len(sys.argv) < 3:
#    print("Usage: annotation.py command config_name <options>")
#    sys.exit()
while True:
    conf_name = input_gse()
    add_gse(conf_name)
    list_annotations(conf_name = conf_name)
    print("Command:")
    print("1: key mapping")
    print("2: value mapping")
    print("3: default value")
    print("4: upload to semares")
    print("5: exit")
    user_input = input(">")
    command = user_input
    if command == "5":
       sys.exit()
    elif command == "1":
        auto_key_mapping(conf_name = conf_name)
    elif command == "2":
        template_fields = get_template_fields()
        print([f"{ind}: {field}" for (ind,field) in enumerate(template_fields)])
        key = input("key>")
        field = list(template_fields)[int(key)]
        auto_value_mapping(conf_name = conf_name, key = field)
    elif command == "3":
        conf = get_config(conf_name)
        template_fields = get_template_fields()
        print([f"{ind}: {field}" for (ind,field) in enumerate(template_fields)])
        key = input("key>")
        field = list(template_fields)[int(key)]
        value = input("value>")
        if "default_values" not in conf:
             conf["default_values"] = {}
        conf["default_values"][field] = value
        save_conf(conf)
#        add_default_value_interactive(conf_name = conf_name, key = key)
    elif command == "4":
        conf_name = input_gse()
        print(conf_name)
        conf = get_config(conf_name)
        save_conf(conf_name, conf) 

#        semares_api_host = "https://semares-api-test.int.ims.bio/dataApi"
#        semares_token = "63b-8_fE_fITNHI983Tst3N2FHthgKGp6lyt9h0QhaFo15Q="
#        project = "sex differences test data v2"
#        experiment_type = "mRNA-seq"
#        experiment_template = "sex_differences_test_data_experiment_template"
#        sample_template = "sex_differences_test_data_sample_template"
        
#        project = " GEO transcriptome data"
#        semares_api_host = "http://localhost:8088/dataApi/"
#        semares_token = "5z7-33b83kNvekmS17ZvylCCunh2LVFPERxA5-Z5OkM4Q1g="
#        experiment_type = "mRNA-seq"
#        experiment_template = "RNA-sequencing"
#        sample_template = "RNA-sequencing_sample"
#        con = connect_semares(semares_api_host, semares_token)
#        gsm_metadata = get_annotations(conf_name = conf_name)
    #    print(gsm_metadata)
#        upload_samples_to_semares(con, gsm_metadata, project, conf_name, experiment_type, experiment_template, sample_template)
    else:
        print(f"Unknown command {command}")


