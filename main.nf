params.output = "./output/"
params.input = "./input/"

input_path = Channel.fromPath( params.input )

process download_annotations {
    container "registry.gitlab.com/semares_pipelines/geo_download/python_geo:1.0.0" // use docker conatainer

    input:
    path input_path

    output:
    path "experiment_data", emit: exp_data
    path "folders.csv", emit: folder_list

    """
     get_annotations.py --output . --input $input_path
    """
}

process download_geo{
    container "registry.gitlab.com/semares_pipelines/geo_download/python_geo:1.0.0" // use docker conatainer
    publishDir params.output, mode: "copy"
 
    input:
    tuple val(sampleId), val(SRR), val(folder)
    path exp_data

    output:
    path exp_data, emit: output_folder

    shell:
    """
        fastq-dump --gzip --skip-technical --readids --read-filter pass --dumpbase --split-3 --clip $SRR
        if [ \$(ls *.fastq.gz | wc -l) == 2 ]
        then
           mv `ls *_pass_1*.fastq.gz` ${sampleId}_S1_L001_R1_001.fastq.gz
           mv `ls *_pass_2*.fastq.gz` ${sampleId}_S1_L001_R2_001.fastq.gz
        else
           mv `ls *.fastq.gz` ${sampleId}_S1_L001_R1_001.fastq.gz
        fi
        mv *.fastq.gz ${exp_data}/sample_data/${folder}/dataset_data
    """  
}

workflow {
  download_annotations(input_path)
  annot_ch = download_annotations.out.folder_list
          | splitCsv(header:true, sep: "\t") \
          | map { row-> tuple(row.sample, row.SRR, row.folder) } \
          | view
  download_geo(annot_ch, download_annotations.out.exp_data.collect())
}