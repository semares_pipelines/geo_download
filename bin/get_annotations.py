#!/usr/bin/env python

import os
import sys
import argparse
import shutil
import json
import platform
import socket
import re
import uuid

import yaml
import GEOparse
import GEOparse.utils as utils
import pandas as pd
import glob

VERSION = 1.0

def option_parser():
    parser = argparse.ArgumentParser(description="Experiment preprocessing")
    parser.add_argument('--input', "-i",
                        help='input path', dest="input_path")
    parser.add_argument('--output', "-o",
                        help='output path', dest="output_path")

    args = parser.parse_args()

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit("Missing parameters!")

    return args


def mkdir(folder):
    if not os.path.exists(folder):
        print(f"mkdir {folder}")
        os.makedirs(folder)

def get_srr(gsm):
    sra = gsm.relations["SRA"][0]
    query = sra.split("=")[-1]
    if "SRX" not in query:
        raise ValueError(
            "Sample looks like it is not an SRA: %s" % query
        )
    answer = utils.esearch(db="sra", term=query, email="maksims.fiosins@gmail.com")
    ids = answer["esearchresult"]["idlist"]
    if len(ids) != 1:
        raise ValueError("There should be one and only one ID per SRX")
    results = utils.efetch(
        db="sra", id=ids[0], rettype="runinfo", email="maksims.fiosins@gmail.com"
                )
    df_tmp = pd.DataFrame(
    [i.split(",") for i in results.split("\n") if i != ""][1:],
    columns=[i.split(",") for i in results.split("\n") if i != ""][
        0
    ],
)
    return df_tmp["Run"][0]

def get_metadata(gse):
    gse = GEOparse.get_GEO(geo=gse, destdir="./")
    metadata = {}
#    cnt = 0
    for gsm_name, gsm in gse.gsms.items():
#        cnt += 1
#        if cnt>2:
#           continue
        metadata[gsm_name] = {}
        for ch in gsm.metadata["characteristics_ch1"]:
            ch_arr = ch.split(":")
            key = ch_arr[0].strip()
            value = ch_arr[1].strip()
            metadata[gsm_name][key] = value
        for key, value in gsm.metadata.items():
            if isinstance(value, list) and len(value)==1:
               value = value[0]
            metadata[gsm_name][key] = value
        metadata[gsm_name]["SRR"] = get_srr(gsm)
    return metadata
    
def process_metadata_fields(metadata, field_map):
    res_meta = {}
    for sample in metadata:
        res_meta[sample] = {}
        for row in field_map:
            if field_map[row] in metadata[sample]:
                res_meta[sample][row] = metadata[sample][field_map[row]]
    return res_meta

def process_metadata_values(metadata, value_map):
    res_meta = {}
    general_key = "__general__"
    for sample in metadata:
        res_meta[sample] = {}
        for field in metadata[sample]:
            value = metadata[sample][field]
            if field in value_map and value in value_map[field]:
               value = value_map[field][value]
            elif general_key in value_map and value in value_map[general_key]:
               value = value_map[general_key][value]
            res_meta[sample][field] = value
    return res_meta

def process_metadata_defaults(metadata, defaults_map):
    res_meta = {}
    for sample in metadata:
        res_meta[sample] = metadata[sample]
        for field in defaults_map:
            if field not in res_meta[sample]:
                res_meta[sample][field] = defaults_map[field]
    return res_meta

def process_metadata(metadata, conf):
    metadata_proc_fields = process_metadata_fields(metadata, conf["field_map"])
    metadata_proc_values = process_metadata_values(metadata_proc_fields, conf["value_map"])
    if "default" in conf:
         metadata_proc_defaults = process_metadata_defaults(metadata_proc_values, conf["default"])
    else:
         metadata_proc_defaults = metadata_proc_values
    return metadata_proc_defaults

def download_fastq(gse, sample_folder_map):
        destdir = "./downloads"
        def filterby(x):
            return (
                x.name in sample_folder_map
            )

        gse = GEOparse.get_GEO(geo=gse, destdir=destdir)
        downloaded_paths = gse.download_SRA(
            "maksims.fiosins@gmail.com",  # some unused e-mail, use your own for run
            directory=destdir,
            filetype="fastq",
            filterby=filterby,
            silent=False,
            keep_sra=False
        )
        download_folders = os.listdir(destdir)
        sample_path_map = {}
        for sample in sample_folder_map:
            for folder in download_folders:
                if sample in folder:
                    sample_path_map[sample] = folder
        print(sample_path_map)
        print(sample_folder_map)
        for sample in sample_path_map:
            cur_path = glob.escape(os.path.join(destdir,sample_path_map[sample]))
            for file in glob.glob(cur_path+"/**/*fastq.gz", recursive = True):
                file_arr = os.path.basename(file).split(".")
                srr = file_arr[0]
                pass_id = file_arr[2].replace("1_pass_","")
                fname = f"{sample}_S1_L0001_R{pass_id}_001.fastq.gz"
                os.rename(file, os.path.join(sample_folder_map[sample], fname))

def create_exp_json(exp_folder):
    conf_name = "object_properties.json" 
    json_output = {
        "level" : "experiment",
        "associations" : [],
        "metadata" : {}
    }
    file = open(os.path.join(exp_folder, conf_name), "w")
    file.write(json.dumps(json_output, indent=2))
    file.close()

def create_json(name, metadata, sample_folder_map):
    exchange_format = "object_properties.json"
    json_output = {
        "level": "sample",
        "name": name,
        "associations": [],
        "metadata": {}
    }
    for meta_field in metadata[name]:
        json_output["metadata"][meta_field] = metadata[name][meta_field]

    file = open(os.path.join(sample_folder_map[name], exchange_format), "w")
    file.write(json.dumps(json_output, indent=2))
    file.close()

def create_map_file(file_name, metadata, sample_folder_map):
    f = open(os.path.join(OUT_PATH, file_name), "w")
    f.write("sample\tSRR\tfolder\n")
    for sample in sample_folder_map:
        folder_name = os.path.basename(sample_folder_map[sample])
        srr = metadata[sample]["SRR"]
        f.write(f"{sample}\t{srr}\t{folder_name}\n")
    f.close()

def split_file():
    with open(f"{INPUT_PATH}", "r") as yaml_file:
        conf = yaml.safe_load(yaml_file)
        gse = conf["GSE"]
        metadata_raw = get_metadata(gse)
        metadata_proc = process_metadata(metadata_raw, conf)
        exp_folder = os.path.join(OUT_PATH, "experiment_data")
        mkdir(exp_folder)
        create_exp_json(exp_folder)
        samp_folder = os.path.join(exp_folder, "sample_data")
        mkdir(samp_folder)
        sample_folder_map = {sample: f"{samp_folder}/sample_data_{ind}" for ind, sample in enumerate(list(metadata_proc.keys()),1)}
        for sample in sample_folder_map:
            mkdir(sample_folder_map[sample])
            create_json(sample, metadata_proc, sample_folder_map)
            mkdir(sample_folder_map[sample]+"/dataset_data")
        create_map_file("folders.csv", metadata_raw, sample_folder_map)
#        download_fastq(GSE, sample_folder_map)

if __name__ == "__main__":
    ARGUMENTS = option_parser()
    INPUT_PATH = ARGUMENTS.input_path
    OUT_PATH = ARGUMENTS.output_path
    split_file()
